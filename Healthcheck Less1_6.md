# Healthcheck
## Run all containers with docker-compose up -d
```bash
[root@CentOS_7 less1_6]# docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
[root@CentOS_7 less1_6]# docker-compose up -d
Creating grafana ... done
Creating nginx2  ... done
Creating nginx1  ... done
```
## Check status
```bash
[root@CentOS_7 less1_6]# docker ps -a
CONTAINER ID   IMAGE               COMMAND                  CREATED          STATUS                    PORTS                                               NAMES
01d383e4bcf4   nginx:1.20-alpine   "/docker-entrypoint.…"   13 seconds ago   Up 11 seconds             80/tcp, 0.0.0.0:8001->8001/tcp, :::8001->8001/tcp   nginx1
7ce525d8db9e   grafana/grafana     "/run.sh"                14 seconds ago   Up 12 seconds (healthy)   3000/tcp                                            grafana
09da07e7afdd   nginx:1.20-alpine   "/docker-entrypoint.…"   14 seconds ago   Up 12 seconds             80/tcp, 0.0.0.0:8002->8002/tcp, :::8002->8002/tcp   nginx2
```
Container nginx1 - __Up__\
Container grafana  - __healthy__
## Stop nginx1 container
```bash
[root@CentOS_7 less1_6]# docker stop nginx1
nginx1
```
## Check status
```bash
[root@CentOS_7 less1_6]# docker ps -a
CONTAINER ID   IMAGE               COMMAND                  CREATED          STATUS                      PORTS                                               NAMES
01d383e4bcf4   nginx:1.20-alpine   "/docker-entrypoint.…"   49 seconds ago   Exited (0) 20 seconds ago                                                       nginx1
7ce525d8db9e   grafana/grafana     "/run.sh"                50 seconds ago   Up 49 seconds (unhealthy)   3000/tcp                                            grafana
09da07e7afdd   nginx:1.20-alpine   "/docker-entrypoint.…"   50 seconds ago   Up 49 seconds               80/tcp, 0.0.0.0:8002->8002/tcp, :::8002->8002/tcp   nginx2
```
Container nginx1 - __Exited__\
Container grafana  - __unhealthy__
## Start nginx1 container
```bash
[root@CentOS_7 less1_6]# docker start nginx1
nginx1
```
## Check status
```bash
[root@CentOS_7 less1_6]# docker ps -a
CONTAINER ID   IMAGE               COMMAND                  CREATED              STATUS                        PORTS                                               NAMES
01d383e4bcf4   nginx:1.20-alpine   "/docker-entrypoint.…"   About a minute ago   Up 10 seconds                 80/tcp, 0.0.0.0:8001->8001/tcp, :::8001->8001/tcp   nginx1
7ce525d8db9e   grafana/grafana     "/run.sh"                About a minute ago   Up About a minute (healthy)   3000/tcp                                            grafana
09da07e7afdd   nginx:1.20-alpine   "/docker-entrypoint.…"   About a minute ago   Up About a minute             80/tcp, 0.0.0.0:8002->8002/tcp, :::8002->8002/tcp   nginx2
```
Container nginx1 - __Up__\
Container grafana  - __healthy__
